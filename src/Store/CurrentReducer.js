const init = 'w';
function CurrentReducer (state = init, action) {
    switch(action.type) {
        case "CHANGE_SIDE":
            return state === 'b'?'w':'b';
        default: 
            return state;
    }
    
}

export default CurrentReducer