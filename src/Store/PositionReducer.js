const init = [];
function PositionReducer (state = init, action) {
    switch(action.type) {
        case 'INIT_POSITION':
            return action.payload;

        case 'MOVE_FIGURE':   
            state.forEach((figure,i) => {
                if(figure.location === action.payload.to) {
                    state.splice(i,1);
                }
                if(figure.location === action.payload.from) {
                    figure.location = action.payload.to;
                    figure.figure.moveCount++;
                };
            })
            return state;
        default: 
            return state;
    }
    
}

export default PositionReducer
