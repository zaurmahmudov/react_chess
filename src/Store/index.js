import { combineReducers } from "redux";
import PositionReducer from "./PositionReducer";
import CurrentFigure from "./CurrentReducer";

const Index = combineReducers({
    positions: PositionReducer,
    current: CurrentFigure
})

export default Index;