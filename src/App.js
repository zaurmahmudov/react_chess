import Board from './Components/Board/Board';
import Store from './Store';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

const store = createStore(
  Store,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
function App() {
  return (
    <Provider store={store} >
      <div className="App">
        <Board/>
      </div>
    </Provider>
  );
}

export default App;
