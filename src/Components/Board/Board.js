import React from 'react';
import Cell from '../Cell/Cell';
import {connect} from 'react-redux';
import './board.scss';
import {init_positions} from '../../Actions/Positions';


class Board extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            cells: [],
            selectedFigure:'',
            pMoves:[],
        }
    }
    componentDidMount() {
        this.props.init_positions()
        setTimeout(()=> {
            this.initCells()
        },10)
        
    }
    selectFigure = (cell, moves) => {
        this.setState({
            selectedFigure:cell,
            pMoves: moves
        })
        setTimeout(()=> {
            this.initCells()
        },10)
    }
    initCells = () => {
        this.setState({
            cells: []
        })
        const positions = this.props.positions;
        const pArr = [];
        positions.forEach(p => {
            pArr[p.location] = p.figure;
        })
        const letters = ['A','B','C','D','E','F','G','H'];
        const numbers = [1,2,3,4,5,6,7,8];
        const wrap = [];
        numbers.reverse().forEach((x,xi) => {
            letters.forEach((y,yi) => {
                let is_black = false;
                if((yi + xi) % 2 !== 0) is_black = true;
                if(pArr[`${y}_${x}`]) {
                    const cell = <Cell key={`${y}_${x}`} x={x} y={y} black={is_black} figure={pArr[`${y}_${x}`]} pMoves={this.state.pMoves} onSelectFigure={this.selectFigure} selected={this.state.selectedFigure} />
                    wrap.push(cell);
                }else {
                    const cell = <Cell key={`${y}_${x}`} x={x} y={y} black={is_black} pMoves={this.state.pMoves} selected={this.state.selectedFigure} onSelectFigure={this.selectFigure}  />
                    wrap.push(cell);
                }
            })
        });
        this.setState({
            cells: wrap
        })
        // return wrap;
    }

    render() {
        return (
            <div className="wrap">
                <div className="wrap__board">
                    <div className="board">
                        {this.state.cells}
                    </div>
                </div>
            </div>
            
        )
    }
}


const mapStateToProps = (state) => {
    return {
        positions: state.positions,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        init_positions:() => dispatch(init_positions()),
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Board)