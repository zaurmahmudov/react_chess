class King {
    constructor(color) {
        this.color = color==='black'?'b':'w';

    }
    getName() {
        return 'king';
    }
    imageName = () => {
        return this.getName()+'_'+this.color;
    }
    addMoveCount() {
        this.moveCount++;
    }

    moves = (currentX,currentY, positions) => {
        const letters = ['A','B','C','D','E','F','G','H'];
        const m = [];
        const indexOfY = letters.indexOf(currentY);
        
        // 9 possible moves 
        // up
        m.push(`${letters[indexOfY+1]}_${currentX}`)
        m.push(`${letters[indexOfY+1]}_${currentX+1}`)
        m.push(`${letters[indexOfY+1]}_${currentX-1}`)

        // DOWN
        m.push(`${letters[indexOfY-1]}_${currentX}`)
        m.push(`${letters[indexOfY-1]}_${currentX+1}`)
        m.push(`${letters[indexOfY-1]}_${currentX-1}`)

        // RIGHT
        m.push(`${letters[indexOfY]}_${currentX+1}`)

        // LEFT
        m.push(`${letters[indexOfY]}_${currentX-1}`)

        console.log(m);
        return this.filterMoves(m,positions,this.color);
    }
    
    filterMoves = (moves, positions, color) => {
        const m = [];
        const positionLists = [];
        positions.forEach(element => {
            positionLists[element.location] = element.figure.color;
        });
        moves.forEach(move => {
            if(positionLists[move] && positionLists[move] === color) {

            }else {
                m.push(move)
            }
        })
        return m
    }
}

module.exports = King;