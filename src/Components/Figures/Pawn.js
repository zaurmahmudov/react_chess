class Pawn {
    constructor(color) {
        this.color = color==='black'?'b':'w';
        this.moveCount = 0;
    }
    getName() {
        return 'pawn';
    }
    imageName = () => {
        return this.getName()+'_'+this.color;
    }
    addMoveCount() {
        this.moveCount++;
    }
    moves = (currentX,currentY,positions) => {
        var m = {},
        posM = this.moveCount === 0?2:1;
        const letters = ['A','B','C','D','E','F','G','H'];
        const indexOfY = letters.indexOf(currentY);
        // Up, Eat
        m['u'] = [];
        m['e'] = [];
        if(this.color==='b'){
            if(posM===2){
                m['u'].push(`${currentY}_${currentX-1}`)
                m['u'].push(`${currentY}_${currentX-2}`)
            }else {
                m['u'].push(`${currentY}_${currentX-1}`)
            }
            m['e'].push(`${letters[indexOfY+1]}_${currentX-1}`)
            m['e'].push(`${letters[indexOfY-1]}_${currentX-1}`)
        }else {
            if(posM===2){
                m['u'].push(`${currentY}_${currentX+1}`)
                m['u'].push(`${currentY}_${currentX+2}`)
            }else {
                m['u'].push(`${currentY}_${currentX+1}`)
                
            }
            m['e'].push(`${letters[indexOfY+1]}_${currentX+1}`)
            m['e'].push(`${letters[indexOfY-1]}_${currentX+1}`)
            
        }
        return this.filterMoves(m,positions,this.color);
    }

    filterMoves = (moves, positions, color) => {
        const m = [];
        const positionLists = [];
        positions.forEach(element =>  positionLists[element.location] = element.figure.color);
        moves['u'].forEach(move => {
            if(positionLists[move] && positionLists[move] === color) {
            }else {
                m.push(move)
            }
        })
        moves['e'].forEach(move => {
            if(positionLists[move] && positionLists[move] !== color) m.push(move)
        })
        return m;
    }

}

module.exports = Pawn;