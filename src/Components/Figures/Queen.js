class Queen {
    constructor(color) {
        this.color = color==='black'?'b':'w';

    }
    getName() {
        return 'queen';
    }
    imageName = () => {
        return this.getName()+'_'+this.color;
    }
    addMoveCount() {
        this.moveCount++;
    }
    moves = (currentX,currentY, positions) => {
        const letters = ['A','B','C','D','E','F','G','H'];
        const m = {};
        const indexOfY = letters.indexOf(currentY);
        // UpLeft, UpRight, DownLeft, DownRight
        m['ul'] =[];
        m['ur'] =[];
        m['dl'] =[];
        m['dr'] =[];
        // Left, Right, Up, Down
        m['l'] =[];
        m['r'] =[];
        m['u'] =[];
        m['d'] =[];
        var pX1 = currentX;
        var pX2 = currentX;
        for(var y = indexOfY+1;y<8;y++){
            pX1++
            pX2--
            if(pX1 <= 8) m['ur'].push(`${letters[y]}_${pX1}`)
            if(pX2 >= 1) m['dr'].push(`${letters[y]}_${pX2}`)
        }
        pX1 = currentX;
        pX2 = currentX;
        for(var y = indexOfY - 1;y >= 0;y--){
            pX1++
            pX2--
            if(pX1 <= 8) m['ul'].push(`${letters[y]}_${pX1}`)
            if(pX2 >= 1) m['dl'].push(`${letters[y]}_${pX2}`)
        }
        for(var s=1;s<=8;s++){
            if(currentX > s) m['d'].push(`${currentY}_${s}`)
            if(currentX < s) m['u'].push(`${currentY}_${s}`)
        }
        
        for(var y=0;y<8;y++){
            if(indexOfY > y) m['l'].push(`${letters[y]}_${currentX}`)
            if(indexOfY < y) m['r'].push(`${letters[y]}_${currentX}`)
        }
        return this.filterMoves(m,positions,this.color);
    }
    
    filterMoves = (moves, positions, color) => {
        const m = [];
        const positionLists = [];
        var add = true;
        const ways = ['ur','ul','dr','dl','u','r']
        const waysReverse = ['d','l'];
        positions.forEach(element => {
            positionLists[element.location] = element.figure.color;
        });
        ways.forEach((way) => {
            add = true;
            moves[way].forEach(move => {
                if(positionLists[move] && color !== positionLists[move]){
                    if(add){
                        m.push(move)
                    }
                    add = false;
                }
                if(positionLists[move] && color === positionLists[move]){
                    add = false;
                }
                if(add){
                    m.push(move)
                }
            });
        })
        waysReverse.forEach(way => {
            var add = true;
            moves[way].reverse().forEach(move => {
                if(positionLists[move] && color !== positionLists[move]){
                    if(add){
                        m.push(move)
                    }
                    add = false;
                }
                if(positionLists[move] && color === positionLists[move]){
                    add = false;
                }
                if(add){
                    m.push(move)
                }
            });
        }) 
        return m
    }
    
}

module.exports = Queen;