class Knight {
    constructor(color) {
        this.color = color==='black'?'b':'w';
        this.moveCount = 0;
    }
    getName() {
        return 'knight';
    }
    imageName = () => {
        return this.getName()+'_'+this.color;
    }
    addMoveCount() {
        this.moveCount++;
    }
    moves = (currentX,currentY, positions) => {
        const letters = ['A','B','C','D','E','F','G','H'];
        const m = [];
        const indexOfY = letters.indexOf(currentY);
        
        // 8 possible moves 
        // UP
        m.push(`${letters[indexOfY+1]}_${currentX+2}`)
        m.push(`${letters[indexOfY-1]}_${currentX+2}`)

        // DOWN
        m.push(`${letters[indexOfY+1]}_${currentX-2}`)
        m.push(`${letters[indexOfY-1]}_${currentX-2}`)

        // RIGHT
        m.push(`${letters[indexOfY+2]}_${currentX+1}`)
        m.push(`${letters[indexOfY+2]}_${currentX-1}`)

        // LEFT
        m.push(`${letters[indexOfY-2]}_${currentX+1}`)
        m.push(`${letters[indexOfY-2]}_${currentX-1}`)

        console.log(m);
        return this.filterMoves(m,positions,this.color);
    }
    
    filterMoves = (moves, positions, color) => {
        const m = [];
        const positionLists = [];
        positions.forEach(element => {
            positionLists[element.location] = element.figure.color;
        });
        moves.forEach(move => {
            if(positionLists[move] && positionLists[move] === color) {

            }else {
                m.push(move)
            }
        })
        return m
    }

    
    
}

module.exports = Knight;