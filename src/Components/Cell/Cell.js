import React, { Component } from 'react'
import {connect} from 'react-redux';
import {move_figure, change_side} from '../../Actions/Move';
import './cell.scss'


class Cell extends Component {
    constructor(props) {
        super(props)
        this.state = {
            x: props.x,
            y: props.y,
            figure: props.figure,
            is_selected: false,
            name: props.y+"_"+props.x
        }
    }
    selectFigure = () => {
        if(this.state.figure.color === this.props.current){
            const moves = this.state.figure.moves(this.state.x,this.state.y,this.props.positions);
            this.props.onSelectFigure(this.state.name,moves);
        }
    }
    makeMove = () => {
        if(this.props.selected && this.props.pMoves.includes(this.state.name)) {
            this.props.change_side();
            this.props.move_figure(this.props.selected, this.state.name);
            this.props.onSelectFigure('',[]);
        }
    }

    getFigure = () => {
        if(!!this.state.figure) {
            const figure = this.state.figure;
            return (
                <img className={this.props.selected === this.state.name?"cell--selected":""} alt={`${figure.imageName()}`} src={process.env.PUBLIC_URL+`/imgs/figures/${figure.imageName()}.png`} onClick={(event) => this.selectFigure()}></img>
            );
        }
        return ''
    }
    render() {
        return (
            
            <div className={this.props.black?"cell cell--black":"cell"}>
                <div className={this.props.pMoves.includes(this.state.name)?"cell__move":""} onClick={(event) => this.makeMove()}>
                    {this.getFigure()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        current: state.current,
        positions: state.positions,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        move_figure: (from_p,to_p) => dispatch(move_figure(from_p,to_p)),
        change_side:() => dispatch(change_side())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Cell)