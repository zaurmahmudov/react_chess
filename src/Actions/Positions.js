const King = require('../Components/Figures/King');
const Queen = require('../Components/Figures/Queen');
const Rook = require('../Components/Figures/Rook');
const Bishop = require('../Components/Figures/Bishop');
const Knight = require('../Components/Figures/Knight');
const Pawn = require('../Components/Figures/Pawn');

export const init_positions = () => {
    var positions = [
        {figure: new King('white'),location:"E_1"},
        {figure: new Queen('white'),location:"D_1"},
        {figure: new Bishop('white'),location:"C_1"},
        {figure: new Bishop('white'),location:"F_1"},
        {figure: new Knight('white'),location:"B_1"},
        {figure: new Knight('white'),location:"G_1"},
        {figure: new Rook('white'),location:"A_1"},
        {figure: new Rook('white'),location:"H_1"},
        {figure: new Pawn('white'),location:"A_2"},
        {figure: new Pawn('white'),location:"B_2"},
        {figure: new Pawn('white'),location:"C_2"},
        {figure: new Pawn('white'),location:"D_2"},
        {figure: new Pawn('white'),location:"E_2"},
        {figure: new Pawn('white'),location:"F_2"},
        {figure: new Pawn('white'),location:"G_2"},
        {figure: new Pawn('white'),location:"H_2"},

        {figure: new King('black'),location:"E_8"},
        {figure: new Queen('black'),location:"D_8"},
        {figure: new Bishop('black'),location:"C_8"},
        {figure: new Bishop('black'),location:"F_8"},
        {figure: new Knight('black'),location:"B_8"},
        {figure: new Knight('black'),location:"G_8"},
        {figure: new Rook('black'),location:"A_8"},
        {figure: new Rook('black'),location:"H_8"},
        {figure: new Pawn('black'),location:"A_7"},
        {figure: new Pawn('black'),location:"B_7"},
        {figure: new Pawn('black'),location:"C_7"},
        {figure: new Pawn('black'),location:"D_7"},
        {figure: new Pawn('black'),location:"E_7"},
        {figure: new Pawn('black'),location:"F_7"},
        {figure: new Pawn('black'),location:"G_7"},
        {figure: new Pawn('black'),location:"H_7"},
    ]
    return {
        type: "INIT_POSITION",
        payload: positions,
    }
}