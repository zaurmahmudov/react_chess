export const select_figure = (name,options) => {
    return {
        type: 'SELECT_FIGURE',
        payload: {
            name,
            options
        }
    }
}

export const move_figure = (from, to) => {
    return {
        type: "MOVE_FIGURE",
        payload: {
            from,
            to
        }
    }
}

export const change_side = () => {
    return {
        type: "CHANGE_SIDE",
        payload: {}
    }
}
